#!/bin/bash
until cd /app
do
    echo "Volume not created"
done

echo ""
echo "-=- Starting rsync from host to docker -=-"
echo ""

rsync -avq --exclude-from '/var/www/exclude_start.txt' /app/* /var/www/project --delete --chmod=ugo=rwX
cp /app/_.env /var/www/project/.env
cp /app/_.env.test /var/www/project/.env.test

echo ""
echo "-=- rsync completed file copy from /app/ to /var/www/project -=-"
echo ""

echo ""
echo "-=- Starting composer install -=-"
echo ""

cd /var/www/project/

/var/www/composer_install.sh
/bin/acomposer install

echo ""
echo "-=- Composer install complete -=-"
echo ""

service apache2 stop
service apache2 start

php bin/console d:s:d --force
php bin/console d:s:u --force
php bin/console d:f:l --append

echo ""
echo "-=- Database dropped, updated and fixtures loaded -=-"
echo ""

(/var/www/rsync_loop.sh 2> /dev/null) &
echo ""
echo "-=- rsync script automatically running every 5 seconds -=-"
echo ""

chmod -R 777 /var/www/project

/bin/bash