#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/var/www
rsync -avq --exclude-from '/var/www/exclude.txt' /app/* /var/www/project --delete --chmod=ugo=rwX