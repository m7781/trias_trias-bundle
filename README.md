# TriasBundle

The TriasBundle includes basic theming for admin interfaces and includes Javascript libraries.

Features include:
- [Theming for admin interfaces](docs/theming.md)
- [Various JavaScript libraries](docs/js_libraries.md)
- [Various fonts and icons](docs/fonts.md)
- [Symfony helper services](docs/services.md)

## Installation

Installation of the TriasBundle is very easy, you can install it by using [Composer](https://getcomposer.org/).
```
composer require trias/trias-bundle
```

### Use the bundle in Symfony
Add the bundle to AppKernel.php:
```php
<?php
$bundles = array(
    // ...
    new Trias\TriasBundle\TriasBundle(),
);
```

## Documentation
To learn more about the TriasBundle, read the [documentation](docs/).

## License
This bundle is under the [proprietary license](docs/LICENSE).

## About
The TriasBundle is a bundle of useful libraries and themes created by [Trias Informatica](https://triasinformatica.nl) for use in [Symfony](http://symfony.com) projects.