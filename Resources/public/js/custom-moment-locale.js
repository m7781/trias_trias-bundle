var additionalLongDateFormats = {
    WDM: "dddd D MMMM",
    WDMY: "dddd D MMMM YYYY"
};
var mergedFormat = $.extend(moment.localeData('nl')._longDateFormat, additionalLongDateFormats);

moment.locale('nl', {
    longDateFormat: mergedFormat
});

additionalLongDateFormats = {
    WDM: "dddd, MMMM Do",
    WDMY: "dddd, MMMM Do YYYY"
};
mergedFormat = $.extend(moment.localeData('en')._longDateFormat, additionalLongDateFormats);

moment.locale('en', {
    longDateFormat: mergedFormat
});