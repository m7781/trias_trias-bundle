$(window).on('statechangecomplete', function() {
	if ($('select.select2').length) {
		$('select.select2').select2();
	}

	$('[data-trias-dtp]').each(function() {
		triasDTP(this);
	});

	$('div[data-google-maps]').each(function() {
		if (!$(this).attr('id')) {
			$(this).attr('id', generateGuid());
		}
		createMap($(this).attr('id'));
	});
});

// Set default position for toastr
if (typeof window.toastr !== 'undefined') {
	toastr.options = {"positionClass": "toast-bottom-right"};
}

/**
 * Show a message
 * @param msg
 * @param type      [success,info,warning,error]
 * @param options
 */
var showMessage = function(msg, type, options) {
	toastr.options = $.extend(toastr.options, options);
	if (typeof toastr[type] !== 'undefined')
		toastr[type](msg);
	else
		toastr['info'](msg);
};

/**
 * Alert/Confirm box
 * @type {{options: {alert: {buttons: *[]}, confirm: {buttons: *[]}}, alert: Box.alert, confirm: Box.confirm, create: Box.create, show: Box.show, close: Box.close}}
 */
var Box = {

	options: {
		alert: {
			buttons: [
				{
					class: 'btn-primary',
					title: 'Ok',
					close: true
				}
			]
		},
		confirm: {
			title: '',
			buttons: [
				{
					id	 : 'modalOkButton',
					class: 'btn-primary',
					title: 'Ok',
					close: true
				},
				{
					id   : 'modalCancelButton',
					class: 'btn-default btn-close alertBoxConfirmCloseBtn',
					title: 'Annuleren',
					close: true
				}
			]
		}
	},

	alert: function( msg, options, callback ) {
		if (typeof options === 'function') {
			callback = options;
			options = {};
		}
		options = $.extend({}, Box.options.alert, options);
		Box.create(msg, options, callback);
	},

	confirm: function( msg, options, callback, cancelCallback ) {
		if (typeof options === 'function') {
			if (typeof callback === 'function') {
				cancelCallback = callback;
			}
			callback = options;
			options = {};
		}

		options = $.extend({}, Box.options.confirm, options);
		Box.create(msg, options, callback, cancelCallback);
	},

	create: function( msg, options, callback, cancelCallback ) {
		if (!msg) return false;

		var buttons = '';
		$.each(options.buttons, function(index, value) {
			var cls = value.close === true ? value.class + ' alertBoxCloseBtn' : '';
			buttons += '<button id="' + value.id + '" type="button" class="btn ' + cls + '">' + value.title + '</button> ';
		});

		var title = options.title;
		var html = '<div id="alertBoxModal" class="modal" role="dialog">' +
						'<div class="vertical-alignment-helper">' +
							'<div class="modal-dialog vertical-align-center">' +
								'<div class="modal-center">' +
									'<div class="modal-content">' +
										'<div class="modal-header">' +
											'<button type="button" class="close alertBoxConfirmCloseBtn" data-dismiss="modal">×</button>' +
											'<h3 class="modal-title">' + title + '</h3>' +
										'</div>' +
										'<div class="modal-body"><p>'+msg+'</p></div>'+
										'<div class="modal-footer text-right">' + buttons + '</div>'+
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>';
		Box.show(html);

		$('#alertBoxModal .btn:eq(0)').focus();

		if (callback) {
			$('#alertBoxModal .btn:eq(0)').click(function () {
				callback();
			});
		}

		$('.alertBoxConfirmCloseBtn').click(function() {
			if (typeof cancelCallback === 'function') {
				cancelCallback();
			}
		});

		$('.alertBoxCloseBtn').click(function() {
			Box.close();
		});
	},

	show: function( html ) {
		$('body').append(html);

		$("#alertBoxModal").modal({
			"backdrop"  : "static",
			"keyboard"  : true,
			"show"      : true
		});
	},

	close: function() {
		$('#alertBoxModal').modal('hide');
		$('#alertBoxModal').remove();
	}

};

/**
 * Generate guid
 * @returns {string}
 */
function generateGuid() {
	var S4 = function() {
		return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
	};
	return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

/**
 * Create Trias datetimepicker
 * @param elm
 * @param date
 */
var triasDTP = function(elm, date) {
	var nonDtpData = $(elm).dataWithPrefix(''),
		data = $(elm).dataWithPrefix('dtp'),
		$join = data.join != null ? data.join : null,
		params = {
			minDate: (date != null ? date : null),
		};
	var options = $.fn.extend(params, nonDtpData, data);

	$(elm).triasDatetimePicker(options).on('change', function(e, date) {
		if ($join) {
			$($join).triasDatetimePicker('setMinDate', date);
			$($join).val($(this).val());
		}
	});
};

/**
 * Create a Google Maps
 * @param id
 */
var createMap = function(id) {
	var data = $('#'+id).data();

	if (!data.latitude || !data.longitude) {
		throw "Coordinates not correctly set";
	}

	$('#'+id).css({ height: (data.height != null ? data.height : 300)+'px' });

	var myLatLng = {
			lat: data.latitude,
			lng: data.longitude
		},
		mapDiv = document.getElementById(id);

	var style = [];
	if (typeof data.greyscale == 'undefined' || data.greyscale == 'on') {
		style = [{
			featureType: 'all',
			elementType: 'all',
			stylers: [{
				saturation: -100
			}]
		}];
	}

	var map = new google.maps.Map(mapDiv, {
		center: myLatLng,
		zoom: (data.zoom != null ? data.zoom : 15),
		scrollwheel: (data.scrollwheel != null ? data.scrollwheel : false),
		styles: style
	});

	var image = '';
	var marker = new google.maps.Marker({
		position: myLatLng,
		map: map,
		icon: image
	});
};

(function($) {
	$.fn.dataWithPrefix = function (prefix) {
		prefix = prefix.replace(/-([a-z])/ig, function (m, $1) {
			return $1.toUpperCase();
		});

		var $data = $(this).data(),
			dataSelection = {};

		this.each(function () {
			Object.keys($data).forEach(function(key) {
				if (key.indexOf(prefix) > -1) {
					var newKey = key.replace(prefix, '', key);
					newKey = newKey.substr(0,1).toLowerCase() + newKey.substr(1);
					dataSelection[newKey] = ($data[key] === '' ? true : $data[key]);
				}
			});
		});

		return dataSelection;
	};
})(jQuery);