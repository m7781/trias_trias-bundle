var $body = $('body');
var $wrapper = $('.content-wrapper');
var $sidebar = $('.main-sidebar');

/**
 * Load URL via pjax
 */
$(function() {
    var Trias = function() {
        $(window).trigger('statechangecomplete');
    };
    window.Trias = new Trias();
});

$sidebar.on('click', 'a[href!="#"]', function() {
    if(!$(this).closest('.treeview-menu').hasClass('menu-open')) {
        $(this).parents('.main-sidebar').find('.active .treeview-menu').slideUp($.AdminLTE.options.animationSpeed, function () {
            $(this).removeClass('menu-open');
        });
    }
    $(this).parents('.main-sidebar').find('.active').removeClass('active');
    $(this).parents('li').addClass('active');
});

var currentPath = location.pathname;
$sidebar.find('a[href="'+currentPath+'"]').each(function() {
    $(this).parents('li').addClass('active');
});

/**
 * Bind ajax call to form
 */
$body.on('submit', 'form.ajax', function(e) {
    var callback = $(this).data('callback') != null ? window[$(this).data('callback')] : null,
        options = { success: callback };
    $(this).ajaxSubmit(options);

    e.preventDefault();
    return false;
});

/**
 * Select all checkbox
 */
$wrapper.on('change', '#selectAll', function() {
    var target = $(this).data('target'),
        callback = $(this).data('callback');
    $(target).prop('checked', $(this).prop('checked'));

    if (typeof window[callback] === 'function') {
        window[callback]();
    }
});
