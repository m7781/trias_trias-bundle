<?php

namespace Trias\TriasBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class TriasExtension extends Extension {
	/**
	 * Loads a specific configuration.
	 *
	 * @param array $configs An array of configuration values
	 * @param ContainerBuilder $container A ContainerBuilder instance
	 *
	 * @throws \InvalidArgumentException When provided tag is not defined in this extension
	 */
	public function load(array $configs, ContainerBuilder $container)
	{
		$loader = new YamlFileLoader(
			$container,
			new FileLocator(__DIR__ . '/../Resources/config')
		);

		$configuration = $this->getConfiguration($configs, $container);
		$config = $this->processConfiguration($configuration, $configs);

		if (isset($config['services'])) {
			// Start parsing services

			if (isset($config['services']['password_resetting_listener'])) {
				// Start parsing password resetting listener services
				if ($this->isConfigEnabled($container, $config['services']['password_resetting_listener'])) {
					// Check for corresponding route
					if (isset($config['services']['password_resetting_listener']['post_reset_route']) && $config['services']['password_resetting_listener']['post_reset_route'] !== null) {
						$container->setParameter('post_reset_route', $config['services']['password_resetting_listener']['post_reset_route']);
						$loader->load('password_resetting.yml');
					} else {
						throw new LogicException('A post_reset_route parameter is required in order to use the PasswordResettingListener service.');
					}
				}
			}

		}
	}
}