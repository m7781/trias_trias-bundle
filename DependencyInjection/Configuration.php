<?php

namespace Trias\TriasBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface {
	/**
	 * Generates the configuration tree builder.
	 *
	 * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
	 */
	public function getConfigTreeBuilder()
	{
		$treeBuilder = new TreeBuilder();
		$rootNode = $treeBuilder->root('trias');

		$this->addPasswordResettingListenerSection($rootNode);

		return $treeBuilder;
	}

	private function addPasswordResettingListenerSection(ArrayNodeDefinition $rootNode) {
		$rootNode
			->children()
				->arrayNode('services')
					->children()
						->arrayNode('password_resetting_listener')
							->canBeEnabled()
							->children()
								->scalarNode('post_reset_route')
									->info('Set to the route you want to redirect the user to after succesfully resetting their password. Using the default value ensures default FOSUserBundle redirect behaviour.')
									->defaultNull()
								->end()
							->end()
						->end() // password_resetting_listener
					->end()
				->end() // services
			->end()
		;
	}
}