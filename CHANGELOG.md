# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [4.0.5] - 2020-01-24
### Changed
- Simplified Bundle structure.

## [4.0.4] - 2020-01-24
### Fixed
- Simplified composer.json
- Removed deprecated calls in Commands.
- Outdated docker.

## [4.0.3] - 2020-01-24
### Fixed
- Composer.json

## [4.0.2] - Yanked
### Fixed
- Missing changelog.

## [4.0.1] - Yanked
### Fixed
- Trailing comma

## [4.0] - Yanked
### Changed
- Migrated to Symfony 4.4

### Added
- Added FontAwesome 5.12.0 Pro.

### Removed
- Pjax
- FOSUserBundle

### Deprecated
- FontAwesome 4 is now deprecated in favour of FontAwesome 5.
- Glyphicons.
- Material icons.

## [3.0-alpha] - 2018-07-18
### Changed
- Migrated to Symfony 3.4

### Removed
- All tests and related dependencies

## [2.0.20] - 2018-01-19
### Fixed
- Issue where the custom moment locale formats are overwritten locales other than English.

## [2.0.19] - 2018-01-19
### Removed
- Basic model clearing since it breaks TriasBooking.

## [2.0.18] - 2018-01-19
### Added
- Support for Pjax asset versioning.

## [2.0.17] - 2017-12-07
### Added
- [Internal] Support for Symfony 3.* projects.

## [2.0.16] - 2017-11-22
### Added
- TriasDateTimePicker 'dateSelectedByUser' callback.

## [2.0.15] - 2017-11-15
### Added
- Custom Moment locales.
- Documentation entry for javascript usage.
- [Internal] Docker support.

### Fixed
- Issue where the browser back button crashes any previously initialized DataTables (QD)

## [2.0.14] - 2017-08-23
### Added
- Added PasswordResettingListener which can be used as a service in Symfony to define the redirect route

## [2.0.13] - 2017-08-15
### Added
- Form error messages are now passed through translations.

### Fixed
- T/V discrepancy.

## [2.0.12] - 2017-08-18
### Fixed
- Missing changelog entry for 2.0.12
- TriasDateTimePicker now works as expected with regards to showing and hiding date and time information based on data attributes.

## [2.0.12] - 2017-08-15
### Fixed
- T/V discrepancy
- Translated form errors

## [2.0.11] - 2017-08-11
### Added
- Clear data for basicModal when closing.

## [2.0.10] - 2017-08-08
### Fixed
- Styling of error pages included an erroneous reference in CSS.

## [2.0.9] - 2017-08-08
### Changed
- Updated styling of error pages to be in line with previous designs.

## [2.0.8] - 2017-08-07
### Added
- Stylized error pages and an exception-handling controller.

## [2.0.7] - 2017-07-28
### Fixed
- Hotfixed Toastr

## [2.0.6] - 2017-06-21
### Changed
- TimeStampableTrait behavior changed to correctly update the UpdatedAt field.

## [2.0.5] - 2017-06-19
### Added
- Cancel callback added.

### Changed
- Browser back button should now work correctly again. May require per-project changes to properly destroy and reinitialize JavaScript plugins.

## [2.0.4] - 2017-04-19
### Changed
- Reverted symfony/console back to 2.8.*.

## [2.0.3] - 2017-04-19 - YANKED
### Changed
- Updated symfony/console to 3.2.* in order to support products that depend on 3.* components.

## [2.0.2] - 2017-03-21
### Fixed
- Removed default load data commands in LoadData.php to prevent erroneous fixtures loading.

## [2.0.1] - 2017-03-21
### Fixed
- Moved test-only Entities into a different directory; this removes the bogus tables that were being created.

## [2.0.0] - 2017-03-09
### Changed
- License changed to proprietary. Licensing options are available.

## [1.3.10] - 2017-03-09
### Changed
- Fixed code that was accidentally deprecated.
- Added: TriasDateTimePicker now expects a 'dtp' prefix for all data-attributes. Ex: ```data-format``` is now ```data-dtp-format```.

### Deprecated
- TriasDateTimePicker data-attributes without dtp prefix.

## [1.3.9] - 2017-03-06
### Added
- Option to add a back button in the TriasDateTimePicker.
- Symfony2 helper console commands.
- TriasDateTimePicker: triggers event "timeSelected" when minute is selected

### Fixed
- Incorrect use of ternary operator.
- Sidebar sub-menu items don't stay uncollapsed.

### Changed
- The login username/email is now an email type input.
- The order of CSS loaded in the base.html.twig.

### Removed
- Removed all JS and CSS map files

## [1.3.8] - 2017-01-12
### Added
- FOSUserBundle templates
- RandExp.js for creating random strings that match a given regular expression.
- New login page styling.

### Fixed
- Bug in sidebar menu causing collapse to behave unexpectedly.

## [1.3.7] - 2016-12-21
### Changed
- TriasDateTimePicker: Automatically set the value of 'joined' input field to value of 'master' input field.

## [1.3.6] - 2016-12-21
### Fixed
- Documentation regarding TriasDateTimePicker was missing an import statement.
- TriasDateTimePicker was ignoring attributes passed via data-attributes due to incorrect use of ternary operators.

## [1.3.5] - 2016-12-02
### Fixed
- Added a third parameter to the showMessage function to customize toastr options

## [1.3.4] - 2016-11-11
### Fixed
- Broken commit causing failed installs

## [1.3.3] - 2016-11-09 [yanked]
### Fixed
- Erroneous console.log statement removed.

## [1.3.2] - 2016-11-04
### Added
- Added "changeDate(date)" function to TriasDateTimePicker to change date programatically.

### Fixed
- Fix for triggering "dateSelected" when changing month or year in TriasDateTimePicker

## [1.3.1] - 2016-11-03
### Fixed
- Fix for incorrect processing TriasDateTimePicker on input elements.

## [1.3.0] - 2016-11-03
### Added
- Inline styling available for TriasDateTimePicker.

## [1.2.0] - 2016-10-31
### Added
- Added form labels to the default theming.
- Added README.md and docs for current functionality.

## [1.1.0] - 2016-10-25
### Added
- Added TimeStampableTrait which can be used in Entities to have them support the `createdAt` and `updatedAt` database fields automatically.

## [1.0.3] - 2016-10-20
### Fixed
- Changed DataTables English translations to be in actual English instead of Dutch

## [1.0.2] - 2016-10-19
### Changed
- default form styles to show errors properly when using form_widget.

## [1.0.1] - 2016-10-10
### Added
- robot noindex to base.html.twig to avoid search engine indexing of admin pages.
### Removed
- http-equiv from base.html.twig for old browser support.

## [1.0.0] - 2016-09-29 (initial release)
### Added
- [jQuery](https://jquery.com/) 2.2.4
- [Bootstrap](http://getbootstrap.com/) 3.3.6
- TriasDateTimePicker
- [Bootstrap DateTimePicker](https://eonasdan.github.io/bootstrap-datetimepicker/) 4.17.37
- [Select2](https://select2.github.io/) 4.0.3
- [Highcharts](http://www.highcharts.com/) 4.2.3
- [DataTables](https://datatables.net/) 1.10.9
- [slimScroll](https://github.com/rochal/jQuery-slimScroll) 1.3.3
- [pjax](https://github.com/defunkt/jquery-pjax) 1.9.6
- [toastr](https://github.com/CodeSeven/toastr) 2.1.2
- [FullCalendar](https://fullcalendar.io/) 2.9.1
- [Bootstrap Switch](http://www.bootstrap-switch.org/) 3.2.2
- [Moment](http://momentjs.com/) 2.11.2
- [JqueryUI](https://jqueryui.com/) 1.11.4
- [FontAwesome](http://fontawesome.io/) 4.6.3
- [OpenSans font](https://fonts.google.com/specimen/Open+Sans)
- [Roboto font](https://fonts.google.com/specimen/Roboto)
- Basic theming for admin interfaces
- On demand javascript loading