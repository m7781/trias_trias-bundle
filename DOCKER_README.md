# Docker README
This file describes the steps required to work with Docker in this project.

## Installation
Before Docker can be used Hyper-V virtualization should be enabled (both in BIOS and Windows).
Download and install the [community edition:](https://store.docker.com/editions/community/docker-ce-desktop-windows?tab=description)

## Configuration
Share your drive where your project is located. Right-click on the Docker icon and select “Settings…”.
Open the tab “Shared drives”, select the drive and press “Apply”.

The apache2 server hosts itself at the ``project.local`` domain, so make sure to add the following line to your hosts file:
```
# C:\Windows\System32\drivers\etc\hosts
...
127.0.0.1    project.local
...
```

Before git cloning, set up correct global git checkout options on your system in order to maintain Linux style line endings (LF) instead of the default Windows style line endings (CRLF) which break the virtual machine. If you have cloned the repo already; remove the folder, apply the config and clone again.

```bash
git config --global core.autocrlf false
git config --global core.eol lf
```

The MySQL and phpMyAdmin containers expect the following Symfony configuration settings:
```
# app/config/parameters.yml
...
database_host: mysql
database_port: null
database_name: symfony
database_user: symfony
database_password: symfony
...
```

## Building and running the Docker containers
Open Windows Powershell, go to your projects directory and run the following command:
```
docker-compose build
```
Afterwards, run the following command:
```
docker-compose run -e TERM --service-ports server
```
Congratulations, your Docker containers are now running.

You can find phpMyAdmin running at [http://project.local:8080/](http://project.local:8080/).

## Code execution in the Docker containers
To start a shell in any given container:
```
docker exec -it <NAME_OF_CONTAINER> bash
```

To see the name of all active containers:
```
docker ps
```

To rebuild all containers:
```
docker-compose build
```
(optionally pass the --no-cache flag for a complete rebuild)

## Helpful Docker commands

```
acomposer       # Wrapper script around composer <x> commands, without synchronization to host
admd            # Generate Doctrine migration, synchronize back to host
akey            # Generates/shows OpenSSH RSA public key
anightwatch     # Run all Nightwatch tests in test environment (configurable through --env=<env>)
asyncstart      # Start periodic rsync script to synchronize changes from host to container
asyncstop       # Stop periodic rsync script to synchronize changes from host to container
asyncuploads    # Sync all uploads from container to host
atestcoverage   # Run all PHPUnit tests in parallel using all available cores and generate coverage in the /coverage folder
atestfast       # Run all PHPUnit tests in parallel using all available cores
atestslow       # Run all PHPUnit tests
composer        # Wrapper script around composer <x> commands, includes synchronization back to host
```