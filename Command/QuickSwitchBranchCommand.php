<?php

namespace Trias\TriasBundle\Command;

use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class QuickSwitchBranchCommand
 * @package Trias\TriasBundle\Command
 */
class QuickSwitchBranchCommand extends Command
{
	private $force = false;

	protected function configure()
	{
		$this->setName('app:quick-switch-branch');
		$this->setDescription('A command that chains the \'drop all tables\', \'create schema\', \'inform database all migrations are loaded\' and \'load fixtures\' commands sequentially.');
		$this->addOption('force', null, InputOption::VALUE_NONE, 'Set this parameter to execute all actions without asking for confirmation.');
		$this->setHelp('Drop all tables, creates schema, informs database all migrations are loaded, load fixtures, sequentially.');
	}

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws Exception
     */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln(array(
			'Performing \'Switch Branch\' procedure...',
			'Running',
			'    [\'doctrine:schema:drop --full-database\'],',
			'    [\'doctrine:schema:create\'],',
			'    [\'doctrine:migrations:version --add --all\'],',
			'    [\'doctrine:fixtures:load\'] sequentially...'
		));
		if (!$input->getOption('force')) {
			$helper = $this->getHelper('question');
			$question = new ConfirmationQuestion('Do you understand the consequences and are you sure you want to continue? ', false);

			if (!$helper->ask($input, $output, $question)) {
				return;
			}
		} else {
			$this->force = true;
		}

		$progress = new ProgressBar($output, 4);
		$output->setVerbosity(OutputInterface::VERBOSITY_VERBOSE);
		$progress->start();

		$command = $this->getApplication()->find('doctrine:schema:drop');
		$commandArgs = array(
			'command' => 'doctrine:schema:drop',
            '--full-database' => true,
		);
		if ($this->force) $commandArgs['--force'] = true;
		$commandInput = new ArrayInput($commandArgs);
		//$output->writeln('Running ['.$commandArgs['command'].'] command...');
		$commandReturnCode = $command->run($commandInput, $this->force === true ? new NullOutput() : $output);
		if (!($commandReturnCode == 0)) {
			$output->writeln('An error occurred while executing ['.$commandArgs['command'].'], exiting.');
			return;
		}

		$progress->setProgress(1);

		$command = $this->getApplication()->find('doctrine:schema:create');
		$commandArgs = array(
			'command' => 'doctrine:schema:create'
		);
		$commandInput = new ArrayInput($commandArgs);
		if ($this->force) $commandInput->setInteractive(false);

		$commandReturnCode = $command->run($commandInput, $this->force === true ? new NullOutput() : $output);
		if (!($commandReturnCode == 0)) {
			$output->writeln('An error occurred while executing ['.$commandArgs['command'].'], exiting.');
			return;
		}

		$progress->setProgress(2);

		$command = $this->getApplication()->find('doctrine:migrations:version');
		$commandArgs = array(
			'command' => 'doctrine:migrations:version',
			'--add' => true,
			'--all' => true,
		);
		$commandInput = new ArrayInput($commandArgs);
		if ($this->force) $commandInput->setInteractive(false);

		$commandReturnCode = $command->run($commandInput, $this->force === true ? new NullOutput() : $output);
		if (!($commandReturnCode == 0)) {
			$output->writeln('An error occurred while executing ['.$commandArgs['command'].'], exiting.');
			return;
		}

		$progress->setProgress(3);

		$command = $this->getApplication()->find('doctrine:fixtures:load');
		$commandArgs = array(
			'command' => 'doctrine:fixtures:load'
		);
		$commandInput = new ArrayInput($commandArgs);
		if ($this->force) $commandInput->setInteractive(false);

		$commandReturnCode = $command->run($commandInput, $this->force === true ? new NullOutput() : $output);
		if (!($commandReturnCode == 0)) {
			$output->writeln('An error occurred while executing ['.$commandArgs['command'].'], exiting.');
			return;
		}
		
		$progress->setProgress(4);

		$output->writeln('');
		$output->writeln('All commands executed successfully.');
	}
}