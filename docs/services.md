### Commands

#### Available commands
- SwitchBranchCommand (app:switch-branch)
- QuickSwitchBranchCommand (app:quick-switch-branch)

#### SwitchBranchCommand
A helper Symfony console command to drop all database tables, run all Doctrine migrations and loads fixtures on the database associated with the Symfony bundle.

Include:
```yaml
# /app/config/services.yml
 
app.trias.switch_branch_command:
    class: Trias\TriasBundle\Command\SwitchBranchCommand
    tags:
        - { name: console.command }
```

Usage:
```
$ php app/console app:switch-branch [--force]
```

| Argument | Parameter | Description |
|-----------|-------------|---------------|
| force | N/a | If this arguments is added, command proceeds without asking for user interaction for any of the commands. |

#### QuickSwitchBranchCommand
A helper Symfony console command to drop all database tables, create a new database schema, inserts complete list of existing migrations into the database and loads fixtures on the database associated with the Symfony2 bundle.

Include:
```yaml
# /app/config/services.yml
 
app.trias.quick_switch_branch_command:
    class: Trias\TriasBundle\Command\QuickSwitchBranchCommand
    tags:
        - { name: console.command }
```

Usage:
```
$ php app/console app:quick-switch-branch [--force]
```

| Argument | Parameter | Description |
|-----------|-------------|---------------|
| force | N/a | If this arguments is added, command proceeds without asking for user interaction for any of the commands. |