## Javascript
The basic theming is based on jQuery and jQueryUI using pjax.

### Available libraries
- [jQuery](https://jquery.com/) 2.2.4
- [jQueryUI](https://jqueryui.com/) 1.11.4
- TriasDateTimePicker 1.0.0
- [Bootstrap DateTimePicker](https://eonasdan.github.io/bootstrap-datetimepicker/) 4.17.37
- [Select2](https://select2.github.io/) 4.0.3
- [Highcharts](http://www.highcharts.com/) 4.2.3
- [DataTables](https://datatables.net/) 1.10.9
- [slimScroll](https://github.com/rochal/jQuery-slimScroll) 1.3.3
- [toastr](https://github.com/CodeSeven/toastr) 2.1.2
- [FullCalendar](https://fullcalendar.io/) 2.9.1
- [Bootstrap Switch](http://www.bootstrap-switch.org/) 3.2.2
- [Moment](http://momentjs.com/) 2.11.2
- [RandExp](http://fent.github.io/randexp.js/) 0.4.3

### Basic usage
Include the library.js in your HTML or [extend the bundle's base.html.twig file](docs/theming.md).

```HTML
<script src="{{ asset('bundles/trias/js/library.js') }}"></script>
```

### Ajax forms
By default, all forms will trigger a page reload after submitting the form. In order to use forms with pjax, give the form a class "ajax".
Now the form will be sent using pjax and no page reload is required. 

This requires logic in the server in the form of an action that handles the form submission via AJAX.

```HTML
<form class="ajax" method="post" action="/" data-callback="functionName">
    Form elements here...
</form>
```

With the data-callback attribute, the given function will be used as a callback.

```javascript
var functionName = function(response) {};
```

The callback will be triggered after successfully submitting the form. The return value of the form action is declared as the parameter "response".

### Standard functions
Include the general.js in your HTML [extend the bundle's base.html.twig file](docs/theming.md).
```HTML
<script src="{{ asset('bundles/trias/js/general.js') }}"></script>
```

#### TriasDateTimePicker
By giving an input the attribute "data-trias-dtp" the input will be rendered as a TriasDateTimePicker.
It can be configured with extra data attributes. All attributes are optional.

In order to work with the TriasDateTimePicker, you need to include the js file.
```html
<script src="{{ asset('bundles/trias/js/datetimepicker.js') }}"></script>
```

Minimum working example:
```html
<input type="input" data-trias-dtp>
```

The following parameters can be used by adding them as a data attribute to the element with the prefix "dtp".
For example:
```html
<input type="input" data-trias-dtp data-dtp-time="false">
```

##### Parameters

| Name         | Type                   | Description                   | Default value    |
|--------------|------------------------|-------------------------------|------------------|
| format       | string                 | MomentJS Format               | DD-MM-YYYY HH:mm |
| week-start   | boolean                | Set the first day of the week | 1 (monday)       |
| lang         | locale                 | Used language                 | nl               |
| short-time   | integer                | true => Display 12 hours AM   | false            |
| min-date     | (string\|date\|moment) | Minimum selectable date       | null             |
| max-date     | (string\|date\|moment) | Maximum selectable date       | null             |
| current-date | (string\|date\|moment) | Initial date                  | The current date |
| date         | boolean                | Show datepicker               | true             |
| time         | boolean                | Show timepicker               | true             |
| back-button  | boolean                | Show the back/cancel button   | false            |
| cancel-text  | string                 | Text on the cancel button     | Cancel           |
| back-text    | string                 | Text on the back button       | Back             |
| join         | string                 | Join an input (CSS selector)  | null             |

##### Events

| Name         | Parameters            | Description          |
|--------------|-----------------------|----------------------|
| beforeChange | event, date           | Date or time has been selected |
| change       | event, date           | Date or time has been selected and changed |
| dateSelected | event, date           | New date is selected |
| dateSelectedByUser | event, date           | New date is selected via explicit user action |
| timeSelected | event, minutes, hours | New time is selected |
| open         | event                 | datepicker is opened |
| close        | event                 | datepicker is closed |

##### Methods

| Name         | Parameter             | Description          |
|--------------|-----------------------|----------------------|
| setDate      | (string\|date\|moment)  | Set initial date     |
| setMinDate   | (string\|date\|moment)  | Set minimum selectable date |
| setDate      | (string\|date\|moment)  | Set maximum selectable date |
| destroy      | null                  | Destroy the datepicker |


#### Google Maps
By giving a div the attribute "data-google-maps" it will be rendered as a Google Map.
There are two mandatory data attributes: data-latitude and data-longitude. Without these attributes the script will throw an error.

Minimum working example:
```html
<div data-google-maps data-latitude="12.34" data-longitude="56.78">
```

It can be configured with extra data attributes. These attributes are optional.

| Attribute | Description | Default value |
|-----------|-------------|---------------|
| height | Set the height of the map | 300 |
| greyscale | Turn off the greyscale | on |
| zoom | The zoom level | 15 |
| scrollwheel | Enable the scrollwheel | false |

#### Toastr
To create a message with toastr, you can use a JavaScript helper function
```javascript
showMessage(message, type)
```
Where message is mandatory and type is optional (default: info), which defines the message type (success, info, warning, error).

#### Select2
All select elements with the class "select2" will be rendered as a Select2 field. No extra configuration available yet.

#### Box
To create a custom alert of confirm box, Box can be used. 


For an alert use:
```javascript
Box.alert(message, options, callback)
```

For a confirm box use:
```javascript
Box.confirm(message, options, callback)
```

| Parameter | Description |
|-----------|-------------|
| message | The message for the alert of confirm box |
| options | Optional: customize the classes / titles |
| callback | Optional: trigger a function after confirmation |