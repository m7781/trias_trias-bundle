## Fonts
- [FontAwesome](http://fontawesome.io/) 5.12.0
- [FontAwesome (deprecated)](http://fontawesome.io/) 4.6.3
- [OpenSans font](https://fonts.google.com/specimen/Open+Sans)
- [Roboto font](https://fonts.google.com/specimen/Roboto)