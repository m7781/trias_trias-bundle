## Theming
The theme available in is based on [AdminLTE](https://almsaeedstudio.com/themes/AdminLTE/index2.html).
This theme uses [Bootstrap](http://getbootstrap.com/) (3.3.6).

### Form
To make use of the custom form theme, add the following in your config.yml:
```
twig:
    form_themes:      ['TriasBundle::forms.html.twig']
```

### Interface theming
This bundle includes a basic theme with various blocks that can be used. See below for a working example and a list of options.

Minimum working example:
```html
{% set title = 'Bundle title' %}
{% extends 'TriasBundle::base.html.twig' %}
 
{% block body %}
    {# include body content of your application here #}
{% endblock %}
```

Full list of options:
```html
{% set title = 'Bundle title' %}
{% extends 'TriasBundle::base.html.twig' %}
 
{% block stylesheets %}
    {# include CSS stylesheets that are not included in the TriasBundle here #}
    <link href="mystylesheet.css" rel="stylesheet">
{% endblock %}
 
{% block javascripts %}
    {# include JavaScript files that are not included in the TriasBundle here #}
    <script id="myScriptId" src="myscript.js"></script>
{% endblock %}
 
{% block header %}
    {# include custom header here #}
{% endblock %}
 
{% block sidebar %}
    {# include sidebar menu here #}
{% endblock %}
 
{% block controlSidebar %}
    {# include custom control sidebar menu here #}
{% endblock %}
 
{% block footer %}
    {# include custom footer here #}
{% endblock %}
 
{% block body %}
    {# include body content of your application here #}
{% endblock %}
```

**_Notice: all Javascript files should be loaded in the base.html.twig. If it is needed to load specific code on a page, this could be done by adding the Javascript to the HTML._**